import { Route } from "react-router";
import { createBrowserRouter, createRoutesFromElements } from "react-router-dom";
import ErrorPage from "./pages/error/Error";
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import TryLogin from "./pages/login/TryLogin";
import Root from "./pages/root/Root";

export const router = createBrowserRouter(
    createRoutesFromElements(
        <Route
            path="/"
            element={<Root />}
            errorElement={<ErrorPage />}
        >
            <Route
                path="/login"
                element={<Login />}>
            </Route>
            <Route
                path="/try-login"
                element={<TryLogin />}>
            </Route>
            <Route
                path="/home"
                element={<Home />}>
            </Route>
        </Route>
    )
);
