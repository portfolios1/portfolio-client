import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const href = `https://github.com/login/oauth/authorize?scope=user:email&client_id=${process.env.REACT_APP_GITHUB_CLIENT_ID}`
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <p>
          We're going to now talk to the GitHub API. Ready?
          <a href={href}>Click here</a> to begin!
        </p>
      </header>
    </div>
  );
}

export default App;
