import { Outlet } from "react-router-dom";

export default function(){
    return (
        <div id="root">
            <Outlet />
        </div>
    )
}