export default function Login(){
    const href = `${process.env.REACT_APP_GITHUB_LOGIN_URL}scope=user:email&client_id=${process.env.REACT_APP_GITHUB_CLIENT_ID}`
    return (
        <div>
            Portfolio Builder Login:
            <a href={href}>Login</a>
        </div>
    )
};